import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;


import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

public class MainTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setup() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "d:/Program Files/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait (10, TimeUnit.SECONDS);
        driver.get("http://software-testing.ru/forum/");
    }

   public void openLoginForm () throws InterruptedException {
       WebElement homeButton = driver.findElement(By. id("logo"));
       homeButton.click();

       Thread.sleep(2000);

        WebElement signInButton = driver.findElement(By. id("sign_in"));
        if (signInButton.isDisplayed()) {
            System.out.println("SignInDisplayed");
        }
        signInButton.click();
        driver.manage().timeouts().implicitlyWait (10, TimeUnit.SECONDS);
        Thread.sleep(2000);
        WebElement loginFormPopUp = driver.findElement(By.id("sign_in_popup_popup"));
        if (loginFormPopUp.isDisplayed()) {
            System.out.println("loginFormPopUpDisplayed");
        }
    }

    private void loginEnterName(String loginName){
         driver.manage().timeouts().implicitlyWait (15, TimeUnit.SECONDS);
          WebElement loginField = driver.findElement(By.id("ips_username"));
        if (loginField.isDisplayed()) {
            System.out.println("loginFieldDisplayed");
        }
          loginField.click();
          loginField.sendKeys(loginName);
}
    private void loginEnterPassword(String loginPassword){
        driver.manage().timeouts().implicitlyWait (15, TimeUnit.SECONDS);
        WebElement passwordField = driver.findElement(By.id("ips_password"));
        if (passwordField.isDisplayed()) {
            System.out.println("passwordFieldDisplayed");
        }
        passwordField.click();
        passwordField.sendKeys(loginPassword);
    }

    private void clickSubmit(){
        driver.manage().timeouts().implicitlyWait (15, TimeUnit.SECONDS);
        WebElement submitButton = driver.findElement(By.className("ipsButton"));
        if (submitButton.isDisplayed()) {
            System.out.println("submitButtonDisplayed");
        }
        submitButton.click();
    }

    private void testUserLogin (String name, String password) throws InterruptedException {
        loginEnterName(name);
        Thread.sleep(2000);
        loginEnterPassword (password);
        Thread.sleep(2000);
        clickSubmit();
    }

    @Test
    public void userValidLogin () throws InterruptedException {
        openLoginForm();
        testUserLogin("AutoTesterJava", "qwer1234");
        driver.manage().timeouts().implicitlyWait (15, TimeUnit.SECONDS);
        WebElement profileUser = driver.findElement(By.id("user_link"));
        String userName = profileUser.getText();
        Assert.assertEquals("AutoTesterJava  ", userName);
        WebElement logoutButton = driver.findElement(By.xpath("//*[@id=\"user_navigation\"]/ul/li[4]/a"));
        if (logoutButton.isDisplayed()) {
            System.out.println("TEST_PASSED");
        }
        logoutButton.click();
        Thread.sleep(2000);
        WebElement signInButton = driver.findElement(By. id("sign_in"));
        if (signInButton.isDisplayed()) {
            System.out.println("User_was_logged_out");
        }
    }

    @Test
    public void userInvalidLoginName () throws InterruptedException {
        openLoginForm();
        testUserLogin("Tester", "qwer1234");
        driver.manage().timeouts().implicitlyWait (15, TimeUnit.SECONDS);
        WebElement errorMessage = driver.findElement(By.cssSelector("p.message.error"));
        Assert.assertTrue (errorMessage.isDisplayed());
    }

    @Test
    public void userInvalidLoginPassword () throws InterruptedException {
        openLoginForm();
        testUserLogin("AutoTesterJava", "12345678");
        driver.manage().timeouts().implicitlyWait (15, TimeUnit.SECONDS);
        WebElement errorMessage = driver.findElement(By.cssSelector("p.message.error"));
        Assert.assertTrue (errorMessage.isDisplayed());
    }

    @Test
    public void userEmptyLoginName () throws InterruptedException {
        openLoginForm();
        testUserLogin("", "qwer1234");
        driver.manage().timeouts().implicitlyWait (15, TimeUnit.SECONDS);
        WebElement errorMessage = driver.findElement(By.cssSelector("p.message.error"));
        Assert.assertTrue (errorMessage.isDisplayed());
    }

    @Test
    public void userEmptyLoginPassword () throws InterruptedException {
        openLoginForm();
        testUserLogin("AutoTesterJava", "");
        driver.manage().timeouts().implicitlyWait (15, TimeUnit.SECONDS);
        WebElement errorMessage = driver.findElement(By.cssSelector("p.message.error"));
        Assert.assertTrue (errorMessage.isDisplayed());
    }
}